package ru.t1.ytarasov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.ytarasov.tm.dto.response.system.ApplicationVersionResponse;
import ru.t1.ytarasov.tm.event.ConsoleEvent;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String DESCRIPTION = "Show application version";

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[VERSION]");
        @NotNull final ApplicationVersionRequest request = new ApplicationVersionRequest();
        @NotNull final ApplicationVersionResponse response = getSystemEndpoint().getVersion(request);
        @Nullable final String version = response.getVersion();
        System.out.println("ClientApplication version: " + version);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
