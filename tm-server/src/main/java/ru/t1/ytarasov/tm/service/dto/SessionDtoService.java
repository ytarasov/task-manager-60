package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.ytarasov.tm.api.service.dto.ISessionDtoService;
import ru.t1.ytarasov.tm.exception.entity.SessionNotFoundException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.repository.dto.SessionDtoRepository;

import java.util.Collection;
import java.util.List;

@Service
public class SessionDtoService
        extends AbstractUserOwnedDtoService<SessionDTO, SessionDtoRepository> implements ISessionDtoService {

    @NotNull
    @Autowired
    private ISessionDtoRepository repository;

    @Nullable
    @Override
    public List<SessionDTO> findAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public Long getSize() throws Exception {
        return repository.getSize();
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO add(@Nullable SessionDTO session) throws Exception {
        if (session == null) throw new SessionNotFoundException();
        repository.add(session);
        return session;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<SessionDTO> add(@NotNull Collection<SessionDTO> models) throws Exception {
        if (models.isEmpty()) throw new SessionNotFoundException();
        for (@NotNull final SessionDTO model : models) add(model);
        return models;
    }

    @Nullable
    @Override
    @Transactional
    public SessionDTO findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.clear();
    }

    @Override
    public Boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO remove(@Nullable SessionDTO session) throws Exception {
        if (session == null) throw new SessionNotFoundException();
        repository.remove(session);
        return session;
    }

    @Nullable
    @Override
    @Transactional
    public SessionDTO removeById(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final SessionDTO session = findOneById(id);
        if (session == null) throw new SessionNotFoundException();
        repository.removeById(id);
        return session;
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public Boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    public Long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    public  SessionDTO removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final SessionDTO session = findOneById(userId, id);
        if (session == null) throw new SessionNotFoundException();
        repository.removeById(id, userId);
        return session;
    }

    @Nullable
    @Override
    @Transactional
    public SessionDTO add(@Nullable String userId, @Nullable SessionDTO model) throws Exception {
        if (model == null) throw new SessionNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    public SessionDTO remove(@Nullable String userId, @Nullable SessionDTO model) throws Exception {
        if (model == null) throw new SessionNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final SessionDTO session = findOneById(userId, model.getId());
        if (session == null) throw new SessionNotFoundException();
        return remove(session);
    }

}
