package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.api.repository.model.ITaskRepository;
import ru.t1.ytarasov.tm.api.service.model.ITaskService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public final class TaskService
        extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @Nullable
    @Override
    public List<Task> findAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public Long getSize() throws Exception {
        return repository.getSize();
    }

    @NotNull
    @Override
    @Transactional
    public Task add(@Nullable Task model) throws Exception {
        if (model == null) throw new TaskNotFoundException();
        repository.add(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public Collection<Task> add(@Nullable Collection<Task> models) throws Exception {
        if (models == null) throw new TaskNotFoundException();
        for (@NotNull final Task model : models) add(model);
        return models;
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.clear();
    }
    @Nullable
    @Override
    @Transactional
    public Task remove(@Nullable Task model) throws Exception {
        if (model == null) throw new TaskNotFoundException();
        repository.remove(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public Task removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        repository.removeById(id);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task update(@Nullable Task model) throws Exception {
        if (model == null) throw new TaskNotFoundException();
        model.setUpdated(new Date());
        repository.update(model);
        return model;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable Sort sort) throws Exception {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable final Comparator comparator) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        return repository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll();
        return repository.findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public List<Task> findByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @Transactional
    public Task create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @NotNull
    @Override
    @Transactional
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return update(task);
    }

    @NotNull
    @Override
    @Transactional
    public Task changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return update(task);
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    public Long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    public Task removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        repository.removeById(id, userId);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task add(@Nullable String userId, @Nullable Task task) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        repository.add(userId, task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public Task remove(@Nullable String userId, @Nullable Task model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        @Nullable final Task task = findOneById(userId, model.getId());
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

}
