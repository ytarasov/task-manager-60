package ru.t1.ytarasov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.api.EntityConstant;
import ru.t1.ytarasov.tm.api.repository.model.IProjectRepository;
import ru.t1.ytarasov.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public final class ProjectRepository
        extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM Project p", Long.class)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM Project p WHERE p.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Long getSize(@NotNull final String userId) {
        @NotNull final String jpql = String.format("SELECT COUNT(p) FROM %s p WHERE p.user.id = :userId",
                getTableName());
        return entityManager
                .createQuery("SELECT COUNT(p) FROM Project p WHERE p.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT COUNT(1) = 1 FROM %s m WHERE m.id = :id AND m.user.id = :userId",
                getTableName());
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM Project p WHERE p.id = :id AND p.user.id = :userId", Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final Comparator comparator) {
        return entityManager
                .createQuery("SELECT p FROM Project p ORDER BY :sort", Project.class)
                .setParameter("sort", getSortedType(comparator))
                .getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId, @NotNull final Comparator comparator) {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY :sort", Project.class)
                .setParameter("userId", userId)
                .setParameter("sort", getSortedType(comparator))
                .getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        return entityManager
                .createQuery("FROM Project", Project.class)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p WHERE p.%s = :id",
                getTableName(), EntityConstant.COLUMN_ID);
        return entityManager
                .createQuery(jpql, Project.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public @Nullable List<Project> findAll(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.id = :id AND p.user.id = :userId", Project.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @NotNull
    @Override
    protected String getTableName() {
        return EntityConstant.TABLE_PROJECT;
    }

}
