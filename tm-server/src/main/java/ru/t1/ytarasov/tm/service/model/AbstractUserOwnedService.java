package ru.t1.ytarasov.tm.service.model;

import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.ytarasov.tm.api.service.model.IUserOwnedService;
import ru.t1.ytarasov.tm.model.AbstractUserOwnedModel;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {
}
