package ru.t1.ytarasov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends IDtoRepository<M> {

    void removeById(@NotNull final String id, @NotNull final String userId);

    void clear(@NotNull final String userId);

    @Nullable
    List<M> findAll(@NotNull final String userId);

    @Nullable
    M findOneById(@NotNull final String userId, @NotNull final String id);

    Long getSize(@NotNull final String id);

    Boolean existsById(@NotNull final String userId, @NotNull final String id);

}
