package ru.t1.ytarasov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) throws Exception;

    @Nullable
    User findByLogin(@Nullable final String login) throws Exception;

    @Nullable
    User findByEmail(@Nullable final String email) throws Exception;

    @Nullable
    User removeByLogin(@Nullable final String login) throws Exception;

    void lockUser(@Nullable final String login) throws Exception;

    void unlockUser(@Nullable final String login) throws Exception;

    void changePassword(@Nullable final String id, @Nullable final String newPassword) throws Exception;

    void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception;

    boolean isLoginExist(@Nullable String login) throws Exception;

    boolean isEmailExists(@Nullable String email) throws Exception;

}
