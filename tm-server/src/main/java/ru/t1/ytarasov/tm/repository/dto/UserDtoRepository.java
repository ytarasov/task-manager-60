package ru.t1.ytarasov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.ytarasov.tm.dto.model.UserDTO;

import java.util.List;

@Repository
@Scope("prototype")
public final class UserDtoRepository
        extends AbstractDtoRepository<UserDTO> implements IUserDtoRepository {

    @Override
    public Long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(u) FROM UserDTO u", Long.class)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM UserDTO u WHERE u.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() {
        return entityManager
                .createQuery("FROM UserDTO", UserDTO.class)
                //.setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    public @Nullable UserDTO findOneById(@NotNull String id) {
        return entityManager
                .createQuery("SELECT u FROM UserDTO u WHERE u.id = :id", UserDTO.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("SELECT u FROM UserDTO u WHERE u.login = :login", UserDTO.class)
                .setParameter("login", login)
                .getSingleResult();
    }

    @Override
    public @Nullable UserDTO findByEmail(@NotNull String email) {
        return entityManager
                .createQuery("SELECT u FROM UserDTO u WHERE u.email = :email", UserDTO.class)
                .setParameter("email", email)
                .getSingleResult();
    }

    @Override
    public void removeByLogin(@NotNull String login) {
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return;
        remove(user);
    }

    @Override
    public Boolean isLoginExists(@NotNull final String login) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM UserDTO u WHERE u.login = :login", Boolean.class)
                .setParameter(EntityConstantDTO.COLUMN_LOGIN, login)
                .getSingleResult();
    }

    @Override
    public Boolean isEmailExists(@NotNull final String email) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM UserDTO u WHERE u.email = :email", Boolean.class)
                .setParameter(EntityConstantDTO.COLUMN_EMAIL, email)
                .getSingleResult();
    }

    @Override
    protected @NotNull String getTableName() {
        return EntityConstantDTO.TABLE_USER;
    }

}
