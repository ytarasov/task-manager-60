package ru.t1.ytarasov.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.ytarasov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.ytarasov.tm.repository.dto.AbstractUserOwnedDtoRepository;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO, R extends AbstractUserOwnedDtoRepository<M>>
        extends AbstractDtoService<M, R> implements IUserOwnedDtoService<M> {
}
