package ru.t1.ytarasov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;

import java.util.Comparator;
import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDTO> {

    @Nullable
    List<TaskDTO> findAll(@NotNull final Comparator comparator);

    @Nullable
    List<TaskDTO> findAll(@NotNull final String userId, @NotNull final Comparator comparator);

    @NotNull
    List<TaskDTO> findAllTasksByProjectId(@NotNull String userId, @NotNull String projectId);

}
