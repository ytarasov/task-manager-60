package ru.t1.ytarasov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.ytarasov.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO>
        extends AbstractDtoRepository<M> implements IUserOwnedDtoRepository<M> {

    @Override
    public void removeById(@NotNull String id, @NotNull String userId) {
        @NotNull final String jpql = String.format("DELETE FROM %s m WHERE m.%s = :id AND m.%s = :userId", getTableName(), EntityConstantDTO.COLUMN_ID,  EntityConstantDTO.COLUMN_USER_ID);
        entityManager
                .createQuery(jpql)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = String.format("DELETE FROM %s m WHERE m.%s = :userId", getTableName(), EntityConstantDTO.COLUMN_USER_ID);
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
