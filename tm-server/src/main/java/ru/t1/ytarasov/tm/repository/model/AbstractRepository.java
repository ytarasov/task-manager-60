package ru.t1.ytarasov.tm.repository.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.api.EntityConstant;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.model.IRepository;
import ru.t1.ytarasov.tm.comparator.CreatedComparator;
import ru.t1.ytarasov.tm.comparator.NameComparator;
import ru.t1.ytarasov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected String getSortedType(@NotNull final Comparator comparator) {
        if (comparator == NameComparator.INSTANCE) return EntityConstant.COLUMN_NAME;
        else if (comparator == CreatedComparator.INSTANCE) return EntityConstant.COLUMN_CREATED;
        else return EntityConstant.COLUMN_STATUS;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(entityManager.contains(model) ? model : entityManager.merge(model));
    }

    @Override
    public void removeById(@NotNull String id) {
        @NotNull final String jpql = String.format("DELETE FROM %s m WHERE m.%s = :id", getTableName(), EntityConstant.COLUMN_ID);
        entityManager
                .createQuery(jpql)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void clear() {
        @NotNull final String jpql = String.format("DELETE FROM %s", getTableName());
        entityManager
                .createQuery(jpql)
                .executeUpdate();
    }

}
