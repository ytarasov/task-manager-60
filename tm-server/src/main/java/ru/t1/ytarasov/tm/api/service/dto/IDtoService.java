package ru.t1.ytarasov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

public interface IDtoService<M extends AbstractModelDTO> {

    @Nullable
    List<M> findAll() throws Exception;

    Long getSize() throws Exception;

    @NotNull
    M add(@Nullable M model) throws Exception;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws Exception;

    Boolean existsById(@Nullable String id) throws Exception;

    @Nullable
    M findOneById(@Nullable String id) throws Exception;

    void clear() throws Exception;

    @Nullable
    M remove(M model) throws Exception;

    @Nullable
    M removeById(String id) throws Exception;

}
