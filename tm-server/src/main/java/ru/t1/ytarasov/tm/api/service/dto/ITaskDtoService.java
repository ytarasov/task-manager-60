package ru.t1.ytarasov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;

import java.util.Comparator;
import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDTO> {

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @Nullable
    List<TaskDTO> findAll(@Nullable final Comparator comparator) throws Exception;

    @Nullable
    List<TaskDTO> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) throws Exception;

    @Nullable
    List<TaskDTO> findAll(@Nullable final Sort sort) throws Exception;

    @Nullable
    List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) throws Exception;

    @NotNull
    List<TaskDTO> findAllTasksByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws Exception;

    @NotNull
    TaskDTO update(@NotNull TaskDTO task) throws Exception;

    @NotNull
    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    TaskDTO changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

}
