package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.dto.IUserDtoService;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.exception.user.ExistsEmailException;
import ru.t1.ytarasov.tm.exception.user.ExistsLoginException;
import ru.t1.ytarasov.tm.exception.user.UserNotFoundException;
import ru.t1.ytarasov.tm.dto.model.UserDTO;
import ru.t1.ytarasov.tm.repository.dto.UserDtoRepository;
import ru.t1.ytarasov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public final class UserDtoService
        extends AbstractDtoService<UserDTO, UserDtoRepository> implements IUserDtoService {

    @NotNull
    @Autowired
    private IUserDtoRepository repository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    @Transactional
    public UserDTO add(@Nullable UserDTO user) throws Exception {
        if (user == null) throw new UserNotFoundException();
        repository.add(user);
        return user;
    }

    @Override
    @Transactional
    public @NotNull Collection<UserDTO> add(@NotNull Collection<UserDTO> models) throws Exception {
        for (@NotNull final UserDTO model : models) add(model);
        return models;
    }

    @Override
    public Boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @NotNull
    @Transactional
    public UserDTO create(@Nullable final String login,
                          @Nullable final String password,
                          @Nullable final String email,
                          @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final UserDTO user = new UserDTO(login, HashUtil.salt(propertyService, password), email);
        user.setRole(role);
        return add(user);
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public UserDTO findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        repository.removeByLogin(login);
        return user;
    }

    @Override
    public Long getSize() throws Exception {
        return repository.getSize();
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO remove(@Nullable UserDTO user) throws Exception {
        if (user == null) throw new UserNotFoundException();
        repository.remove(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        repository.removeById(id);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO removeByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return removeByLogin(user.getLogin());
    }

    @NotNull
    @Override
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setUpdated(new Date());
        repository.update(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setUpdated(new Date());
        repository.update(user);
        return user;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.isLoginExists(login);
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.isEmailExists(email);
    }

    @Override
    @Transactional
    public void lockUser(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        user.setUpdated(new Date());
        repository.update(user);
    }

    @Override
    @Transactional
    public void unlockUser(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        user.setUpdated(new Date());
        repository.update(user);
    }

    @Override
    @Transactional
    public void clear() {
        repository.clear();
    }

}
