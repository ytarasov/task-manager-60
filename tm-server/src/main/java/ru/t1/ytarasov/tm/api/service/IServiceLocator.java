package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.service.dto.*;
import ru.t1.ytarasov.tm.api.service.model.IProjectService;
import ru.t1.ytarasov.tm.api.service.model.IProjectTaskService;
import ru.t1.ytarasov.tm.api.service.model.ITaskService;

public interface IServiceLocator {

    @NotNull
    IProjectDtoService getProjectServiceDTO();

    @NotNull
    ITaskDtoService getTaskServiceDTO();

    @NotNull
    IProjectTaskServiceDTO getProjectTaskServiceDTO();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IUserDtoService getUserServiceDTO();

    @NotNull
    ISessionDtoService getSessionServiceDTO();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectTaskService getProjectTaskService();

}
