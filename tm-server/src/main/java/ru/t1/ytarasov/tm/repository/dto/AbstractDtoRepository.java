package ru.t1.ytarasov.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.IDtoRepository;
import ru.t1.ytarasov.tm.comparator.CreatedComparator;
import ru.t1.ytarasov.tm.comparator.NameComparator;
import ru.t1.ytarasov.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;

@Getter
@Repository
@Scope("prototype")
public abstract class AbstractDtoRepository<M extends AbstractModelDTO> implements IDtoRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected String getSortedColumn(@NotNull final Comparator comparator) {
        if (comparator == NameComparator.INSTANCE) return EntityConstantDTO.COLUMN_NAME;
        else if (comparator == CreatedComparator.INSTANCE) return EntityConstantDTO.COLUMN_CREATED;
        else return EntityConstantDTO.COLUMN_STATUS;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(entityManager.contains(model) ? model : entityManager.merge(model));
    }

    @Override
    public void removeById(@NotNull String id) {
        @NotNull final String jpql = String.format("DELETE FROM %s m WHERE m.%s = :id", getTableName(), EntityConstantDTO.COLUMN_ID);
        entityManager
                .createQuery(jpql)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = String.format("DELETE FROM %s", getTableName());
        entityManager.createQuery(jpql).executeUpdate();
    }

}
