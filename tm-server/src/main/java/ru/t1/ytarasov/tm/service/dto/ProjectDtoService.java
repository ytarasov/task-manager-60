package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.ytarasov.tm.api.service.dto.IProjectDtoService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;
import ru.t1.ytarasov.tm.repository.dto.ProjectDtoRepository;

import java.util.*;

@Service
public final class ProjectDtoService
        extends AbstractUserOwnedDtoService<ProjectDTO, ProjectDtoRepository> implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @Override
    @NotNull
    @Transactional
    public ProjectDTO add(@Nullable final ProjectDTO project) throws Exception {
        if (project == null) throw new ProjectNotFoundException();
        repository.add(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> models) throws Exception {
        for (ProjectDTO model : models) add(model);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO updateById(@Nullable final String userId,
                                 @Nullable final String id,
                                 @Nullable final String name,
                                 @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        project.setUpdated(new Date());
        return update(project);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.clear();
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO(name, description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO changeProjectStatusById(@Nullable final String userId,
                                              @Nullable final String id,
                                              @Nullable final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null || status.getDisplayName().isEmpty()) throw new StatusEmptyException();
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        project.setUpdated(new Date());
        return update(project);
    }

    @Override
    @Transactional
    public @NotNull ProjectDTO update(@Nullable ProjectDTO project) throws Exception {
        if (project == null) throw new ProjectNotFoundException();
        project.setUpdated(new Date());
        repository.update(project);
        return project;
    }

    @Override
    public @Nullable List<ProjectDTO> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@Nullable Sort sort) throws Exception {
        if (sort == null) return findAll();
        else return findAll(sort.getComparator());
    }

    @Override
    public Long getSize() throws Exception {
        return repository.getSize();
    }

    @Override
    public Long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        else return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO remove(@Nullable final ProjectDTO project) throws Exception {
        if (project == null) throw new ProjectNotFoundException();
        repository.remove(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        repository.removeById(id);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        repository.removeById(id, userId);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ProjectNotFoundException();
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO remove(@Nullable String userId, @Nullable ProjectDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ProjectNotFoundException();
        @Nullable final ProjectDTO projectDTO = findOneById(userId, model.getId());
        if (projectDTO == null) throw new ProjectNotFoundException();
        return remove(projectDTO);
    }

    @Override
    public Boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    public Boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.existsById(userId, id);
    }

}
