package ru.t1.ytarasov.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.dto.IDtoService;
import ru.t1.ytarasov.tm.dto.model.AbstractModelDTO;
import ru.t1.ytarasov.tm.repository.dto.AbstractDtoRepository;

@Service
public abstract class AbstractDtoService<M extends AbstractModelDTO, R extends AbstractDtoRepository<M>> implements IDtoService<M> {
}
